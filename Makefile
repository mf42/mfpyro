SHELL := /bin/bash

include config
include config.local

SRC = ${BOOTSTRAP} ${EXTRA_SOURCE_FILES}

NEW_HASH = $(shell cat ${SRC} | sha256sum | cut -f1 -d' ' )
NEW_IMAGE = $(shell date +"%y%m%d")_$(shell echo ${NEW_HASH} | head -c4)

CUR_IMAGE_DIR = ${IMAGE}
NEW_IMAGE_DIR = ${NEW_IMAGE}

NEW_IMAGE_FILE = ${NEW_IMAGE_DIR}/${NAME}_${NEW_IMAGE}.img
CUR_IMAGE_FILE = ${CUR_IMAGE_DIR}/${NAME}_${IMAGE}.img

PYKERNEL = ~/.local/share/jupyter/kernels/${NAME}_python_${IMAGE}/
RKERNEL = ~/.local/share/jupyter/kernels/${NAME}_r_${IMAGE}/



info:
	@echo "Base name           : ${NAME}"
	@echo "Current image hash  : ${IMAGE}"
	@echo "New image hash      : ${NEW_IMAGE}"
	@echo "New image file      : ${NEW_IMAGE_FILE}"
	@echo "Boostrap file       : ${BOOTSTRAP}"
	@echo "Extra source files  : ${EXTRA_SOURCE_FILES}"
	@echo "Singularity         : ${SING}"
	@echo "Singularity fs bind : ${SINGARGS}"
	@echo "Singularity loader  : ${LOAD}"
	@echo "Current image file  : ${CUR_IMAGE_FILE}"
	@echo "New image file      : ${NEW_IMAGE_FILE}"


# Boostrap a new image
image: ${NEW_IMAGE_FILE}

${NEW_IMAGE_FILE}: ${SRC}
	date
	mkdir -p ${NEW_IMAGE_DIR}/singularity
	cp ${SRC} ${NEW_IMAGE_DIR}/singularity/
	@echo "Creating image: ${NEW_IMAGE_FILE}"
	sudo ${SING} build --writable ${NEW_IMAGE_FILE} ${BOOTSTRAP} \
		2> ${NEW_IMAGE_DIR}/singularity/build.err \
		> ${NEW_IMAGE_DIR}/singularity/build.out

	cat printversion.sh \
		| ${SING} exec ${NEW_IMAGE_FILE} bash \
		> ${NEW_IMAGE_DIR}/singularity/VERSION

	date
	echo -e "\n\n# Created image - if ok, run 'make set_new_image'"


version:
	cat printversion.sh | ${SING} exec ${CUR_IMAGE_FILE} bash


set_version:
	echo "changing image in config to latest"
	sed -i.bak 's/IMAGE \?= \?.*/IMAGE = ${NEW_IMAGE}/' config
	(cd ${NEW_IMAGE_DIR}/singularity/; git add ${SRC} VERSION)
	git commit -m 'release ${NEW_IMAGE}' -a
	git tag -a ${NEW_IMAGE} -m '$(shell date +'%Y-%m-%d %H:%M') ${NEW_IMAGE}'



# Prepare environment
.PHONY: env
env:
	chmod a+rX .
	chmod a+rX ${CUR_IMAGE_DIR}
	./create_env -B="${SINGARGS}" -S="${SING}" -L="${LOAD}" -D ${CUR_IMAGE_DIR} ${CUR_IMAGE_FILE} ${EXECUTABLES}
	@echo "To activate: 'source ${CUR_IMAGE_DIR}/activate'"


kernel_python:
	mkdir -p ${PYKERNEL}
	cp ./extra/python-logo-32x32.png ${PYKERNEL}/logo-32x32.png
	cp ./extra/python-logo-64x64.png ${PYKERNEL}/logo-64x64.png
	echo '{"argv": ["${PWD}/${CUR_IMAGE_DIR}/python3.6", "-m", "ipykernel_launcher", "-f", "{connection_file}" ], "display_name": "PY ${NAME} ${IMAGE}", "language": "python" }' \
		> ${PYKERNEL}/kernel.json


kernel_R:
	${SING} exec ${CUR_IMAGE_FILE} R \
		-e 'library(IRkernel); IRkernel::installspec(name="${NAME}_r_${IMAGE}", displayname="R ${NAME} ${IMAGE}")'
	ls ${RKERNEL}
	sed -i.bak 's#/usr/lib/R/bin/R#${PWD}/${CUR_IMAGE_DIR}/R#' ${RKERNEL}/kernel.json

#python_kernel_system:
#	sudo mkdir -p ${PYKERNELSYS}
#	sudo cp ./extra/python-logo-32x32.png ${PYKERNELSYS}/logo-32x32.png
#	sudo cp ./extra/python-logo-64x64.png ${PYKERNELSYS}/logo-64x64.png
#	echo '{"argv": ["${PWD}/${CUR_IMAGE_DIR}/python3.6", "-m", "ipykernel_launcher", "-f", "{connection_file}" ], "display_name": "Python mfpyro.${IMAGE}", "language": "python" }' \
#		> sudo tee ${PYKERNELSYS}/kernel.json


shell:
	${SING} shell ${SINGARGS} ${CUR_IMAGE_FILE}


${CUR_IMAGE_FILE}.overlay:
	${SING} image.create ${CUR_IMAGE_FILE}.overlay


shello: ${CUR_IMAGE_FILE}.overlay
	${SING} shell --overlay ${CUR_IMAGE_FILE}.overlay ${SINGARGS} ${CUR_IMAGE_FILE}


upload:
	curl -o curl.log --user ${PUBUSR} -T '${CUR_IMAGE_FILE}' \
		'${PUBURL}/${NAME}.${IMAGE}.img'

download:
	mkdir -p ${CUR_IMAGE_DIR}
	curl -o ${CUR_IMAGE_FILE} '${GETURL}${NAME}.${IMAGE}.img'
