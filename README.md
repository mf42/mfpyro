# MFPYRO #

Singularity images for bioinformatics

## Prerequisites ##

- Singularity (probably 2.5.2 or higher).
- Gnu Make
- Curl


## INSTALL ##

In a folder with sufficient space:

	git clone https://gitlab.com/mf42/mfpyro.git
	cd mfpyro

Check the current version:

	make info

The version number can be seen under `Current image hash`. You may
want to set a few parameters. Check `config.local`. Parameters that
you may want to change are:

- `SINGARGS`: Typically used to specifiy which filsystem paths need to
  be bound. Default is `-B /mnt,/data` which makes the `/mnt` and
  `/data` directories available from within the containers
- `LOAD`: Is a command that could be required to make singularity
  available on the `PATH`. This is added to the `activate`
  script. (Note, that for running any of the `make` commands described
  here, the `SING` parameter is used:
- `SING`: the path to the singularity version to use.


## Using ##

Once set up, the easiest thing to do is to pull the lastest image from
our nextcloud (easier than building yourself) (note - this may take a
while).

	make download

If you want to check the versions of tools included in the image, run:

	make version


You you can create an `environment` (equivalent to python virtual
environments) to make using the image easier:

	make env


This generates a number of small scripts wrapping the singularity
container. If you want to use this environment (in bash) run:

	source <ENV DIR>/activate


After that you should be able to run the commands transparently.



## Changing ##

If you want to add packages to the image, you can edit the `bootstrap`
file, or the `install_r_mods.sh` for `R` packages. It is probably
advisable to test if the installation works by trying this in a shell
at the same time. You can open a shell by (note you do need root to do
this).

	make shello


After having edited the source files, rebuild the image using (not
from the shell you opened!). Again you will need root to do this.

	make new_image

After the build finishes (wil take a long time), you should the new
image identifier as the current one in the `config` file. Change this line:

	IMAGE = XXXX

ensuring your identifier is the new one. Once set, you can recreate an
environment

	make env

## Install jupyter kernels ##

Install a python kernel (in user space) using:

	make kernel_python

and an R kernel with:

	make kernel_R
