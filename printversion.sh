#!/bin/bash

echo -e "##### SYSTEM\n"
cat /etc/lsb*

samtools
exit

echo -e "\n\n##### Extra packages"

if (which STAR >/dev/null); then STAR --version
else echo -e "\n## STAR is not installed"; fi

if (which featureCounts >/dev/null); then featureCounts -v 2>&1 | grep '.'
else echo -e "\n## featurCounts is not installed"; fi

if (which python3 >/dev/null); then
    echo -e "\n\n##### PYTHON"
    which python3
    python3 --version
    pip3 freeze
else echo -e "\n\n##### Python3 is not installed"; fi


if (which R >/dev/null); then
    echo -e "\n\n##### R"
    R --version
    echo 'ip = as.data.frame(installed.packages()[,c(3)])
    ip' | R --vanilla
else
    echo -e "\n\n##### R NOT INSTALLED"
fi

echo -e "\n\n##### dpk"
dpkg-query --list
